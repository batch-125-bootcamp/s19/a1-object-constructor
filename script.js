// create a set of pokemon for pokemon battle

function Pokemon (name, lvl, hp){
	
	this.name = name;
	this.level = lvl;
	this.health = hp;
	this.attack = lvl; 
	this.tackle = function(opponent){
		
		console.log(`${this.name} tackled ${opponent.name}`)

		opponent.health = opponent.health - this.attack
		console.log(`${opponent.name}'s health is now reduced to ${opponent.health}`)
		this.faint(opponent)
		
	};
		this.faint = function(opponent){
			if (opponent.health <= 10){
			console.log(`${opponent.name} fainted`)
		}
	}
}

let jigglypuff = new Pokemon ("Jigglypuff", 10, 50);

let snorlax = new Pokemon ("Snorlax", 10, 50);

console.log(jigglypuff.tackle(snorlax));
console.log(snorlax.tackle(jigglypuff));
console.log(jigglypuff.tackle(snorlax));
console.log(snorlax.tackle(jigglypuff));
console.log(jigglypuff.tackle(snorlax));
console.log(snorlax.tackle(jigglypuff));
console.log(snorlax.tackle(jigglypuff));